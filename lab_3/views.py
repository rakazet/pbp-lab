from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()  
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST': # Ngecek POST atau GET. POST itu protokol masukin data, GET itu buat ngambil data.
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()  # Simpan data di DB
            return HttpResponseRedirect('/lab-3')  # Redirect kalau sudah selesai

    else:
        form = FriendForm() # Buat form kosong

    return render(request, 'lab3_form.html', {'form':form})