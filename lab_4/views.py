from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all()  
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    if request.method == 'POST': # Ngecek POST atau GET. POST itu protokol masukin data, GET itu buat ngambil data.
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()  # Simpan data di DB
            return HttpResponseRedirect('/lab-4')  # Redirect kalau sudah selesai

    else:
        form = NoteForm() # Buat form kosong

    return render(request, 'lab4_form.html', {'form':form})

def note_list(request):
    notes = Note.objects.all()  
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)