/// Flutter code sample for BottomNavigationBar

// This example shows a [BottomNavigationBar] as it is used within a [Scaffold]
// widget. The [BottomNavigationBar] has three [BottomNavigationBarItem]
// widgets, which means it defaults to [BottomNavigationBarType.fixed], and
// the [currentIndex] is set to index 0. The selected item is
// amber. The `_onItemTapped` function changes the selected item's index
// and displays a corresponding message in the center of the [Scaffold].

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class Checkout extends StatelessWidget {
  const Checkout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(4),
        color: Color.fromRGBO(20, 20, 20, 1),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    const ListTile(
                      tileColor: Color.fromRGBO(50, 55, 62, 1),
                      leading: Icon(Icons.person),
                      title: Text('STEP 1: DETAIL PRIBADI',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1))),
                      subtitle: Text(
                          'Nama Lengkap: Julius Prayoga Raka Nugroho\nEmail: raka.gundam@hotmail.com\nNomor Telepon: 3102213123\nAlamat: AAA',
                          style: TextStyle(
                              color: Color.fromRGBO(240, 240, 240, 1))),
                    ),
                    const ListTile(
                      tileColor: Color.fromRGBO(50, 55, 62, 1),
                      leading: Icon(Icons.local_shipping_outlined),
                      title: Text('STEP 2: METODE PENGIRIMAN',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1))),
                      subtitle: Text(
                          'Durasi: Reguler (2-4 hari)\nHarga Pengiriman: 1 dollar\nKurir: AnterAja',
                          style: TextStyle(
                              color: Color.fromRGBO(240, 240, 240, 1))),
                    ),
                    const ListTile(
                      tileColor: Color.fromRGBO(50, 55, 62, 1),
                      leading: Icon(Icons.attach_money),
                      title: Text('STEP 3: METODE PEMBAYARAN',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1))),
                      subtitle: Text('Metode Pembayaran: BCA Virtual Account',
                          style: TextStyle(
                              color: Color.fromRGBO(240, 240, 240, 1))),
                    ),
                    const ListTile(
                      tileColor: Color.fromRGBO(50, 55, 62, 1),
                      leading: Icon(Icons.check),
                      title: Text('STEP 4: KONFIRMASI ORDER',
                          style: TextStyle(
                              color: Color.fromRGBO(255, 255, 255, 1))),
                      subtitle: Text(
                          'Banyak produk: 1\nHarga total produk: 10 dollar\nHarga akhir: 10 dollar\nNote: None',
                          style: TextStyle(
                              color: Color.fromRGBO(240, 240, 240, 1))),
                    ),
                  ],
                ),
              ),
            )
          ],
        ));
  }
}

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      theme: ThemeData(
        fontFamily: 'Georgia',
      ),
      home: MyStatefulWidget(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    Text(
      'HOME PAGE!',
      style: optionStyle,
    ),
    Checkout()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.add_shopping_cart_rounded),
            label: 'Checkout',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.purple[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
