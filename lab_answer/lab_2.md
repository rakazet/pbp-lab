# Jawaban Pertanyaan Lab 2
**1. Apakah perbedaan antara JSON dan XML?**

JSON dan XML adalah format untuk penyimpanan dan transmisi data. Perbedaan terbesar antara keduanya adalah perbedaan sintaks. Sintaks pada JSON relatif lebih mudah dibanding XML. Hal ini menyebabkan meningkatnya popularitas JSON. Berikut perbedaan antara keduanya:

- Objek JSON memiliki *type*, sedangkan objek XML tidak.
- JSON tidak memiliki *namespaces support*, sedangkan XML memilikinya.
- JSON tidak punya kemampuan untuk menampilkan data, sedangkan XML punya.
- JSON kurang aman dibanding XML.
- JSON hanya mendukung *encoding* UTF-8, sedangkan XML mendukung banyak format *encoding*.
- JSON tidak mendukung *comments*, sementara XML mendukungnya.
- JSON mendukung *array*, sementara XML tidak.
- JSON memiliki struktur data map (adanya pasangan *key-value*), sedangkan data pada XML direpresentasikan dalam bentuk *tree*.

**2. Apakah perbedaan antara HTML dan XML?**

Pada dasarnya HTML dan XML merupakan dua hal dengan tujuan berbeda. HTML adalah sebuah *markup* untuk mengatur bagaimana benda-benda diinterpretasikan pada suatu *webpage*, sedangkan XML adalah format untuk menyimpan dan mentransmisikan data. Berikut perbedaan antara keduanya:

- HTML bersifat *static*, sementara XML bersifat dinamis.
- HTML tidak menyimpan *white spaces*, sementara XML menyimpannya.
- HTML hanya menampilkan data, sementara XML membawa data dari dan ke *database*.
- HTML berfokus pada presentasi data, sementara XML berfokus pada transfer data.
- HTML bersifat *case insensitive*, sementara XML bersifat *case sensitive*.
- HTML tidak memiliki *namespaces support*, sementara XML memilikinya.
- *Tags* pada HTML bersifat *predefined*, sementara pada XML tidak.


## SUMBER
[Guru99 - JSON vs XML](https://www.guru99.com/json-vs-xml-difference.html)

[GeeksforGeeks - JSON vs XML](https://www.geeksforgeeks.org/difference-between-json-and-xml/)

[Guru99 - HTML vs XML](https://www.guru99.com/xml-vs-html-difference.html)

[GeeksforGeeks - HTML vs XML](https://www.geeksforgeeks.org/html-vs-xml/)