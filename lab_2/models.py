from django.db import models

# Create your models here.

class Note(models.Model):
    To = models.CharField(max_length=64)
    From = models.CharField(max_length=64)
    Title = models.CharField(max_length=128)
    Message = models.CharField(max_length=1024)